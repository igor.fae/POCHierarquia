﻿using Microsoft.Extensions.DependencyInjection;
using POC.Hierarquia.API.Data;
using POC.Hierarquia.API.Data.Repository;
using POC.Hierarquia.API.Models;

namespace POC.Hierarquia.API.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IHistoricaLancamentoRepository, HistoricaLancamentoRepository>();
            services.AddScoped<ApplicationDbContext>();
        }
    }
}