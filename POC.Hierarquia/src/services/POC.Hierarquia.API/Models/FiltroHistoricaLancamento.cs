﻿using System;

namespace POC.Hierarquia.API.Models
{
    public class FiltroHistoricaLancamento
    {
        public string Usuario { get; set; }
        public string Diretoria { get; set; }
        public string Superintendencia { get; set; }
        public string Gerencia { get; set; }
        public string Coordenacao { get; set; }
        public string DataContabilInicio { get; set; }
        public string DataContabilFim { get; set; }
        public string DataEmissaoInicio { get; set; }
        public string DataEmissaoFim { get; set; }
    }
}