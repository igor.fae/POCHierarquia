﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace POC.Hierarquia.API.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistoricaLancamentos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Conta = table.Column<string>(type: "varchar(13)", nullable: false),
                    Empresa = table.Column<string>(type: "varchar(6)", nullable: false),
                    Orgao = table.Column<string>(type: "varchar(5)", nullable: false),
                    Valor = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Usuario = table.Column<string>(type: "varchar(7)", nullable: false),
                    Diretoria = table.Column<string>(type: "varchar(100)", nullable: true),
                    Superintendencia = table.Column<string>(type: "varchar(100)", nullable: true),
                    Gerencia = table.Column<string>(type: "varchar(100)", nullable: true),
                    Coordenacao = table.Column<string>(type: "varchar(100)", nullable: true),
                    DataContabil = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DataEmissao = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoricaLancamentos", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistoricaLancamentos");
        }
    }
}
