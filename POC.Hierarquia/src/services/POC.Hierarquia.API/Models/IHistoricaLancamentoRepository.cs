﻿using System.Collections.Generic;
using System.Threading.Tasks;
using POC.Hierarquia.API.Data;

namespace POC.Hierarquia.API.Models
{
    public interface IHistoricaLancamentoRepository : IRepository<HistoricaLancamento>
    {
        Task<IEnumerable<HistoricaLancamento>> Select();
        Task<HistoricaLancamento> Select(int id);
        Task<IEnumerable<object>> SelectDiretoria();
        Task<IEnumerable<object>> SelectSuperintendencia(string diretoria);
        Task<IEnumerable<object>> SelectGerencia(string diretoria, string superintendencia);
        Task<IEnumerable<object>> SelectCoordenacao(string diretoria, string superintendencia, string gerencia);
        Task<IEnumerable<object>> SelectUsuario(string diretoria, string superintendencia, string gerencia, string coordenacao);
        Task<IList<HistoricaLancamento>> Select(FiltroHistoricaLancamento filtro);
    }
}