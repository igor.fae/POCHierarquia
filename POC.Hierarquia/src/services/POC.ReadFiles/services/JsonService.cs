﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Newtonsoft.Json;
using POC.ReadFiles.Models;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace POC.ReadFiles.services
{
    public class JsonService
    {

        private IList<Arquivo> _listOld;
        private string _listNew;
        private Arquivo novoArquivo;

        public async Task<bool> CreateJson(string path)
        {
            if (!File.Exists(path))
            {
                try
                {
                    using (FileStream fs = new FileStream(path, FileMode.Create))
                    {
                        await fs.WriteAsync(Encoding.UTF8.GetBytes("[]"));
                        await fs.DisposeAsync();
                    }
                    return true;
                }
                catch (IOException)
                {
                    return false;
                }
            }
            return true;
        }

        public async Task<string> AddObjectToJson(string dbJson, Arquivo obj)
        {
            await Task.Run(() =>
            {
                try
                {
                    var options = new JsonSerializerOptions
                    {
                        IncludeFields = true,
                    };

                    _listOld = JsonSerializer.Deserialize<IList<Arquivo>>(File.ReadAllText(dbJson), options);

                    if (!_listOld.Any(x => x.Name == obj.Name))
                    {
                        _listOld.Add(obj);
                    }
                    _listNew = JsonSerializer.Serialize<IList<Arquivo>>(_listOld);
                }
                catch (Exception)
                {
                    _listOld.Add(obj);
                    _listNew = JsonSerializer.Serialize<IList<Arquivo>>(_listOld);
                }
            });

            await using FileStream fs = new FileStream(dbJson, FileMode.Open);
            await fs.WriteAsync(Encoding.UTF8.GetBytes(_listNew));
            await fs.DisposeAsync();

            return _listNew;
        }

        public async Task<Arquivo> ReadNew(string dbJson)
        {
            await Task.Run(() =>
            {
                try
                {
                    var options = new JsonSerializerOptions
                    {
                        IncludeFields = true,
                    };

                    _listOld = JsonSerializer.Deserialize<IList<Arquivo>>(File.ReadAllText(dbJson), options);
                    novoArquivo = _listOld.FirstOrDefault(x => x.Status == "novo");
                }
                catch (Exception e)
                {
                    novoArquivo = null;
                }

            });
            return novoArquivo;
        }

        public async Task Update(string dbJson, Arquivo arquivo, string status)
        {
            await Task.Run(() =>
            {
                try
                {
                    var options = new JsonSerializerOptions
                    {
                        IncludeFields = true,
                    };

                    _listOld = JsonSerializer.Deserialize<IList<Arquivo>>(File.ReadAllText(dbJson), options);
                    _listOld.Remove(_listOld.SingleOrDefault(x => x.Id == arquivo.Id));

                    var arquivoAtualizado = arquivo;
                    arquivoAtualizado.Status = status;
                    arquivoAtualizado.UpdateAt = DateTime.UtcNow;
                    _listOld.Add(arquivoAtualizado);

                    _listNew = JsonSerializer.Serialize<IList<Arquivo>>(_listOld);
                }
                catch (Exception e)
                {
                    novoArquivo = null;
                }
            });

            await using FileStream fs = new FileStream(dbJson, FileMode.Open);
            await fs.WriteAsync(Encoding.UTF8.GetBytes(_listNew));
            await fs.DisposeAsync();




        }


    }
}