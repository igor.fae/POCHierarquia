﻿using System;
using System.Threading.Tasks;
using POC.ReadFiles.services;

namespace POC.ReadFiles
{
    class Program
    {
        public static string DBJSON = @"C:\dev\99-Pocs\04-Hierarquia\POC.Hierarquia\files\DB\db.json";
        static async Task Main(string[] args)
        {
            var jsonService = new JsonService();
            await jsonService.CreateJson(DBJSON);
            Console.WriteLine("Iniciando a leitura da pasta de arquivos....");
            var fileService = new FilesServices();
            var arrayFiles = await fileService.SelectAllFilesInFolder(@"C:\dev\99-Pocs\04-Hierarquia\POC.Hierarquia\files\FilesToRead");

            foreach (var file in arrayFiles)
            {
                await jsonService.AddObjectToJson(DBJSON, file);
            }

            var novoArquivo = await jsonService.ReadNew(DBJSON);

            while (novoArquivo != null)
            {
                await jsonService.Update(DBJSON, novoArquivo, "processado");
                novoArquivo = await jsonService.ReadNew(DBJSON);
            }

            var stop = "";
        }
    }
}
