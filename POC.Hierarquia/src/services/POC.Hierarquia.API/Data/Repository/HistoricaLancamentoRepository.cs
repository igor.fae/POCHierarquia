﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using POC.Hierarquia.API.Models;

namespace POC.Hierarquia.API.Data.Repository
{
    public class HistoricaLancamentoRepository : IHistoricaLancamentoRepository
    {
        private readonly ApplicationDbContext _context;

        public HistoricaLancamentoRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<HistoricaLancamento>> Select()
        {
            return await _context.HistoricaLancamentos.AsNoTracking().ToListAsync();
        }

        public async Task<HistoricaLancamento> Select(int id)
        {
            return await _context.HistoricaLancamentos.FindAsync(id);
        }

        public async Task<IEnumerable<object>> SelectDiretoria()
        {
            var diretorias = _context.HistoricaLancamentos
                .Select(h => new
                {
                    h.Diretoria,
                }).Distinct()
                .OrderBy(h => h.Diretoria);
            return await diretorias.ToArrayAsync();
        }

        public async Task<IEnumerable<object>> SelectSuperintendencia(string diretoria)
        {
            var suptes = _context.HistoricaLancamentos
                .Select(h => new
                {
                    h.Diretoria,
                    h.Superintendencia
                }).Where(h => h.Diretoria == diretoria)
                .Distinct()
                .OrderBy(h => h.Superintendencia);
            return await suptes.ToArrayAsync();
        }

        public async Task<IEnumerable<object>> SelectGerencia(string diretoria, string superintendencia)
        {
            var gerencias = _context.HistoricaLancamentos
                .Select(h => new
                {
                    h.Diretoria,
                    h.Superintendencia,
                    h.Gerencia
                }).Where(h => h.Diretoria == diretoria && h.Superintendencia == superintendencia)
                .Distinct()
                .OrderBy(h => h.Gerencia);
            var teste = await gerencias.ToArrayAsync();
            return await gerencias.ToArrayAsync();
        }

        public async Task<IEnumerable<object>> SelectCoordenacao(string diretoria, string superintendencia, string gerencia)
        {
            var coordenacoes = _context.HistoricaLancamentos
                .Select(h => new
                {
                    h.Diretoria,
                    h.Superintendencia,
                    h.Gerencia,
                    h.Coordenacao
                }).Where(h => h.Diretoria == diretoria && h.Superintendencia == superintendencia && h.Gerencia == gerencia)
                .Distinct()
                .OrderBy(h => h.Diretoria);
            return await coordenacoes.ToArrayAsync();
        }

        public async Task<IEnumerable<object>> SelectUsuario(string diretoria, string superintendencia, string gerencia, string coordenacao)
        {
            var usuarios = _context.HistoricaLancamentos
                .Select(h => new
                {
                    h.Usuario,
                    h.Diretoria,
                    h.Superintendencia,
                    h.Gerencia,
                    h.Coordenacao
                }).Where(h => h.Diretoria == diretoria && h.Superintendencia == superintendencia && h.Gerencia == gerencia && h.Coordenacao == coordenacao)
                .Distinct()
                .OrderBy(h => h.Diretoria);
            return await usuarios.ToArrayAsync();
        }

        public async Task<IEnumerable<object>> SelectHierarquic()
        {
            var diretorias = _context.HistoricaLancamentos
                .Select(h => new
                {
                    h.Usuario,
                    h.Diretoria,
                    h.Superintendencia,
                    h.Gerencia,
                    h.Coordenacao
                }).Distinct()
                .OrderBy(h => h.Diretoria);
            return await diretorias.ToArrayAsync();
        }

        public async Task<IList<HistoricaLancamento>> Select(FiltroHistoricaLancamento filtro)
        {
            IQueryable<HistoricaLancamento> historiaLancamentosFiltrada = _context.HistoricaLancamentos;

            if (!string.IsNullOrEmpty(filtro.Diretoria))
                historiaLancamentosFiltrada = historiaLancamentosFiltrada.Where(x => x.Diretoria.ToLower() == filtro.Diretoria.ToLower());
            if (!string.IsNullOrEmpty(filtro.Superintendencia))
                historiaLancamentosFiltrada = historiaLancamentosFiltrada.Where(x => x.Superintendencia.ToLower() == filtro.Superintendencia.ToLower());
            if (!string.IsNullOrEmpty(filtro.Gerencia))
                historiaLancamentosFiltrada = historiaLancamentosFiltrada.Where(x => x.Gerencia.ToLower() == filtro.Gerencia.ToLower());
            if (!string.IsNullOrEmpty(filtro.Coordenacao))
                historiaLancamentosFiltrada = historiaLancamentosFiltrada.Where(x => x.Coordenacao.ToLower() == filtro.Coordenacao.ToLower());
            if (!string.IsNullOrEmpty(filtro.Usuario))
                historiaLancamentosFiltrada = historiaLancamentosFiltrada.Where(x => x.Usuario.ToLower() == filtro.Usuario.ToLower());
            if (!string.IsNullOrEmpty(filtro.DataContabilInicio))
                historiaLancamentosFiltrada = historiaLancamentosFiltrada.Where(x => x.DataContabil >= DateTime.Parse(filtro.DataContabilInicio, new CultureInfo("en")));
            if (!string.IsNullOrEmpty(filtro.DataContabilFim))
                historiaLancamentosFiltrada = historiaLancamentosFiltrada.Where(x => x.DataContabil <= DateTime.Parse(filtro.DataContabilFim, new CultureInfo("en")));
            if (!string.IsNullOrEmpty(filtro.DataEmissaoInicio))
                historiaLancamentosFiltrada = historiaLancamentosFiltrada.Where(x => x.DataEmissao >= DateTime.Parse(filtro.DataEmissaoInicio, new CultureInfo("en")));
            if (!string.IsNullOrEmpty(filtro.DataEmissaoFim))
                historiaLancamentosFiltrada = historiaLancamentosFiltrada.Where(x => x.DataEmissao <= DateTime.Parse(filtro.DataEmissaoFim, new CultureInfo("en")));
            var result = await historiaLancamentosFiltrada.ToListAsync();
            return result;
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}