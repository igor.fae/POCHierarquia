import { RelatorioService } from './../../services/relatorio.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FiltroRelatorio } from 'src/app/models/relatorio';
import { Observable } from 'rxjs';
import { formatDate, KeyValuePipe } from '@angular/common';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.scss'],
  providers: [KeyValuePipe]
})
export class RelatorioComponent implements OnInit {

  allDiretorias: Observable<any[]>;
  allSuperintendencias: Observable<any[]>;
  diretoria: string;
  allGerencias: Observable<any[]>;
  superintendencia: string;
  allCoordenacoes: Observable<any[]>;
  gerencia: string;
  coordenacao: string;
  relatorioForm: FormGroup;
  private dateYesterday: Date = new Date();
  allUsuarios: Observable<any[]>;
  usuario: string;
  filtro: FiltroRelatorio;
  formResult: string = '';

  constructor(private fb: FormBuilder, private relatorioService: RelatorioService) { }

  ngOnInit(): void {
    this.clearForm();
    this.FillDiretoriaDDL();
  }

  filtrar() {
    this.filtro = {
      Usuario: this.usuario,
      Diretoria: this.diretoria,
      Superintendencia: this.superintendencia,
      Gerencia: this.gerencia,
      Coordenacao: this.coordenacao,
      DataContabilInicio: this.relatorioForm.controls['dataContabilInicio'].value,
      DataContabilFim: this.relatorioForm.controls['dataContabilFim'].value,
      DataEmissaoInicio: this.relatorioForm.controls['dataEmissaoInicio'].value,
      DataEmissaoFim: this.relatorioForm.controls['dataEmissaoFim'].value,
    }

    this.relatorioService.download(this.filtro).subscribe((data) => {
      this.downloadFile(data)
    },
      error => alert(`Nenhum registro localizado para essa pesquisa - ${error.status}`));

    //this.filtro = Object.assign({}, this.filtro, this.relatorioForm.value);
    //this.formResult = JSON.stringify(this.relatorioForm.value);
    this.relatorioForm.reset();
    this.diretoria = this.superintendencia = this.gerencia =
      this.coordenacao = this.usuario = null;
    this.clearForm();
  }

  private downloadFile(data: any) {
    const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const url = window.URL.createObjectURL(blob);
    window.open(url);
  }

  FillDiretoriaDDL() {
    this.allDiretorias = this.relatorioService.getDiretorias();
    this.allSuperintendencias = this.superintendencia = this.allGerencias = this.gerencia =
      this.allCoordenacoes = this.coordenacao = this.allUsuarios = this.usuario = null;
  }

  SetInitialValue(field, value) {
    this.relatorioForm.controls[field].setValue(value);
    this[field] = '';
  }

  FillSuperintendencia(diretoria) {
    this.allSuperintendencias = this.relatorioService
      .getSuperintendencias(diretoria.target.value);
    this.diretoria = diretoria.target.value;
    this.allGerencias = this.gerencia = this.allCoordenacoes =
      this.coordenacao = this.allUsuarios = this.usuario = null;
  }

  FillGerencias(superintendencia) {
    this.allGerencias = this.relatorioService
      .getGerencias(this.diretoria, superintendencia.target.value);
    this.superintendencia = superintendencia.target.value;
    this.allCoordenacoes = this.coordenacao = this.allUsuarios = this.usuario = null;
  }

  FillCoordenacoes(gerencia) {
    this.allCoordenacoes = this.relatorioService
      .getCoordenacoes(this.diretoria, this.superintendencia, gerencia.target.value);
    this.gerencia = gerencia.target.value;
    this.allUsuarios = this.usuario = null
  }

  FillUsuarios(coordenacao) {
    this.allUsuarios = this.relatorioService
      .getUsuarios(this.diretoria
        , this.superintendencia
        , this.gerencia
        , coordenacao.target.value
      );
    this.coordenacao = coordenacao.target.value;
  }

  GetSelectedUsuario(usuario) {
    this.usuario = usuario.target.value;
  }

  clearForm() {
    this.dateYesterday = new Date();
    this.dateYesterday = new Date(this.dateYesterday.setDate(this.dateYesterday.getDate() - 1));
    this.relatorioForm = this.fb.group({
      diretoria: ['', [Validators.required]],
      superintendencia: ['', [Validators.required]],
      gerencia: ['', [Validators.required]],
      coordenacao: ['', [Validators.required]],
      usuario: ['', [Validators.required]],
      dataContabilInicio: [`${formatDate(this.dateYesterday, 'MM/dd/yyyy', 'en')}`],
      dataContabilFim: [`${formatDate(this.dateYesterday, 'MM/dd/yyyy', 'en')}`],
      dataEmissaoInicio: [`${formatDate(this.dateYesterday, 'MM/dd/yyyy', 'en')}`],
      dataEmissaoFim: [`${formatDate(this.dateYesterday, 'MM/dd/yyyy', 'en')}`],
    });
  }
}
