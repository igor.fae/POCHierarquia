﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using POC.ReadFiles.Models;

namespace POC.ReadFiles.services
{
    public class FilesServices
    {

        private IList<Arquivo> files = new List<Arquivo>();

        public async Task<IList<Arquivo>> SelectAllFilesInFolder(string folderPath)
        {

            await Task.Run(() =>
            {
                IEnumerable<string> fileEntries = Directory.GetFiles(folderPath, "*.xlsx", SearchOption.TopDirectoryOnly);
                int count = 0;
                foreach (string fname in fileEntries)
                {
                    try
                    {
                        FileInfo file = new FileInfo(fname);
                        files.Add(new Arquivo()
                        {
                            Id = Guid.NewGuid(),
                            Name = file.Name,
                            Path = file.Directory.FullName,
                            Status = "novo",
                            CreateAt = file.CreationTimeUtc,
                            UpdateAt = DateTime.UtcNow,
                            User = "Robot1"
                        });
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

            });
            return files;
        }

        
    }
}