﻿using System;
using POC.Hierarquia.API.Data;

namespace POC.Hierarquia.API.Models
{
    public class HistoricaLancamento : IAggregateRoot
    {
        public int Id { get; set; }
        public string Conta { get; set; }
        public string Empresa { get; set; }
        public string Orgao { get; set; }
        public decimal Valor { get; set; }
        public string Usuario { get; set; }
        public string Diretoria { get; set; }
        public string Superintendencia { get; set; }
        public string Gerencia { get; set; }
        public string Coordenacao { get; set; }
        public DateTime DataContabil { get; set; }
        public DateTime DataEmissao { get; set; }
    }
}