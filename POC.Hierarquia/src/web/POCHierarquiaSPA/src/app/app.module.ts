import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilterUniquePipe } from './components/pipes/filter-unique.pipe';
import { RelatorioComponent } from './pages/relatorio/relatorio.component';
import { RelatorioService } from './services/relatorio.service';

@NgModule({
  declarations: [
    AppComponent,
    RelatorioComponent,
    FilterUniquePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
  ],
  providers: [RelatorioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
