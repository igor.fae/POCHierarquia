﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;
using FastMember;
using OfficeOpenXml;
using POC.Hierarquia.API.Models;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace POC.Hierarquia.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistoricaLancamentoController : Controller
    {
        private readonly IHistoricaLancamentoRepository _historicaLancamentoRepository;

        public HistoricaLancamentoController(IHistoricaLancamentoRepository historicaLancamentoRepository)
        {
            _historicaLancamentoRepository = historicaLancamentoRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<HistoricaLancamento>> Index()
        {
            return await _historicaLancamentoRepository.Select();
        }

        [HttpGet("{id}")]
        public async Task<HistoricaLancamento> GetById(int id)
        {
            return await _historicaLancamentoRepository.Select(id);
        }

        [HttpGet("diretorias")]
        public async Task<IEnumerable<object>> GetDiretorias()
        {
            return await _historicaLancamentoRepository.SelectDiretoria();
        }

        [HttpGet("superintendencias/{diretoria?}")]
        public async Task<IEnumerable<object>> GetSuperintendencias(string diretoria = "")
        {
            return await _historicaLancamentoRepository.SelectSuperintendencia(diretoria);
        }

        // MUDAR PARA FROM QUERY
        [HttpGet("gerencias/{diretoria?}")]
        public async Task<IEnumerable<object>> GetGerencias(string diretoria, [FromQuery] string superintendencia = "")
        {
            return await _historicaLancamentoRepository.SelectGerencia(diretoria, superintendencia);
        }

        [HttpGet("coordenacoes/{diretoria?}")]
        public async Task<IEnumerable<object>> GetCoordenacoes(string diretoria, [FromQuery] string superintendencia = "", [FromQuery] string gerencia = "")
        {
            return await _historicaLancamentoRepository.SelectCoordenacao(diretoria, superintendencia, gerencia);
        }

        [HttpGet("usuarios/{diretoria?}")]
        public async Task<IEnumerable<object>> GetUsuarios(string diretoria, [FromQuery] string superintendencia = "", [FromQuery] string gerencia = "", [FromQuery] string coordenacao = "")
        {
            return await _historicaLancamentoRepository.SelectUsuario(diretoria, superintendencia, gerencia, coordenacao);
        }

        [HttpGet("filtro")]
        public async Task<IEnumerable<HistoricaLancamento>> GetByFilter([FromBody] FiltroHistoricaLancamento filtro)
        {
            return await _historicaLancamentoRepository.Select(filtro);
        }

        [HttpGet("excel"), DisableRequestSizeLimit]
        public async Task<IActionResult> GetExcelFile([FromQuery] string filtro)
        {
            var stream = new MemoryStream();
            var filtroEntity = JsonSerializer.Deserialize<FiltroHistoricaLancamento>(filtro);
            DataTable tabela = await tabela_export(filtroEntity);

            using (ExcelPackage package = new ExcelPackage(stream))
            {
                var ws = package.Workbook.Worksheets.Add("Planilha1");
                ws.Cells.LoadFromDataTable(tabela, true);
                package.Save();
            }

            if (tabela.Rows.Count == 0)
            {
                return NotFound(new {message = "Nenhum registro localizado para essa pesquisa"});
            }

            stream.Position = 0;
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $"teste_{DateTime.Now.ToString("yyyyMMdd")}.xlsx");
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<DataTable> tabela_export(FiltroHistoricaLancamento filtro)
        {
            var relatorio = await _historicaLancamentoRepository.Select(filtro);
            return ToDataTable<HistoricaLancamento>(relatorio);

        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public DataTable ToDataTable<T>(IList<T> data)
        {
            DataTable table = new DataTable();
            using (var reader = ObjectReader.Create(data))
            {
                table.Load(reader);
            }
            return table;
        }

        // Teste
        //{
        //    "usuario": "faeigor",
        //    "diretoria": "DOP",
        //    "superintendencia": "SUPT1",
        //    "gerencia": "GER5",
        //    "coordenacao": "",
        //    "dataContabilInicio": "21/01/2019",  
        //    "dataEmissaoInicio": "22-01-2019",
        //    "dataEmissaoFim": "2019-01-26T00:00:00"
        //}
    }
}
