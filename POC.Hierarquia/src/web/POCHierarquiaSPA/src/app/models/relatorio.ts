export interface Relatorio {
  Id: number;
  Conta: string;
  Empresa: string;
  Orgao: string;
  Valor: number;
  Usuario: string;
  Diretoria: string;
  Superintendencia: string;
  Gerencia: string;
  Coordenacao: string;
  DataContabil: Date;
  DataEmissao: Date;
}

export interface FiltroRelatorio {
  Usuario: string;
  Diretoria: string;
  Superintendencia: string;
  Gerencia: string;
  Coordenacao: string;
  DataContabilInicio: Date;
  DataContabilFim: Date;
  DataEmissaoInicio: Date;
  DataEmissaoFim: Date;
}

export interface Hierarquia {
  Usuario: string;
  Diretoria: string;
  Superintendencia: string;
  Gerencia: string;
  Coordenacao: string;
}
