﻿using System;
using System.Text.Json.Serialization;

namespace POC.ReadFiles.Models
{
    public class Arquivo
    {
        [JsonInclude]
        public Guid Id { get; set; }
        [JsonInclude]
        public string Name { get; set; }
        [JsonInclude]
        public string Path { get; set; }
        [JsonInclude]
        public string User { get; set; }
        [JsonInclude]
        public string Status { get; set; }
        [JsonInclude]
        public DateTime CreateAt { get; set; }
        [JsonInclude]
        public DateTime UpdateAt { get; set; }
        
    }
}