﻿using System;

namespace POC.Hierarquia.API.Data
{
    // Pode ir depois para o projeto CORE dentro de buildingBlocks
    public interface IRepository<T> : IDisposable where T : IAggregateRoot
    {
    }
}