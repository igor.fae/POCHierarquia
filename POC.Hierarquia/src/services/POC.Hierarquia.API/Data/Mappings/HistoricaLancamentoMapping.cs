﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using POC.Hierarquia.API.Models;

namespace POC.Hierarquia.API.Data.Mappings
{
    public class HistoricaLancamentoMapping : IEntityTypeConfiguration<HistoricaLancamento>
    {
        public void Configure(EntityTypeBuilder<HistoricaLancamento> builder)
        {
            builder.ToTable("HistoricaLancamentos");
            builder.HasKey(hl => hl.Id);
            builder.Property(hl => hl.Id).ValueGeneratedOnAdd();
            builder.Property(hl => hl.Conta).IsRequired().HasColumnType("varchar(13)");
            builder.Property(hl => hl.Empresa).IsRequired().HasColumnType("varchar(6)");
            builder.Property(hl => hl.Orgao).IsRequired().HasColumnType("varchar(5)");
            builder.Property(hl => hl.Valor).IsRequired().HasColumnType("decimal(18,2)");
            builder.Property(hl => hl.Usuario).IsRequired().HasColumnType("varchar(7)");
            builder.Property(hl => hl.Diretoria).HasColumnType("varchar(100)");
            builder.Property(hl => hl.Superintendencia).HasColumnType("varchar(100)");
            builder.Property(hl => hl.Gerencia).HasColumnType("varchar(100)");
            builder.Property(hl => hl.Coordenacao).HasColumnType("varchar(100)");
        }
    }
}